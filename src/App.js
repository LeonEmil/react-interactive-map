import React, { Component } from "react";
//import ReactDOM from "react-dom";
import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"

class App extends Component {
      componentDidMount() {

        let scene, cube, camera, renderer, controls

        const init = () => {
          scene = new THREE.Scene();
          scene.background = new THREE.CubeTextureLoader()
          .setPath("https://raw.githubusercontent.com/Daefher/RetroScene/master/assets/Skybox/")
          .load([
            "skybox_right.png",
            "skybox_left.png",
            "skybox_up.png",
            "skybox_down.png",
            "skybox_back.png",
            "skybox_front.png"
          ])
  
          camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
          camera.position.z = 25
          camera.position.y = 15
          
          renderer = new THREE.WebGLRenderer({antialias: true});
          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.setPixelRatio(window.devicePixelRatio)
          renderer.setClearColor("purple")
          document.body.appendChild(renderer.domElement);
  
          let axesHelper = new THREE.AxesHelper()
          scene.add(axesHelper)
          axesHelper.position.y = 5
          
          controls = new OrbitControls(camera, renderer.domElement)
  
          let light = new THREE.DirectionalLight(0xffffff)
          light.position.set(0, 10, 1)
          scene.add(light)
  
          let hemi = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.5)
          hemi.position.set(0, 0, 5)
          scene.add(hemi)
          
          const geometry = new THREE.SphereGeometry( 5, 32, 32 );
          const material = new THREE.MeshBasicMaterial( {color: 0xc553c1} );
          const sphere = new THREE.Mesh( geometry, material );
          scene.add( sphere );
        }

        const animate = () => {
          requestAnimationFrame(animate);

          renderer.render(scene, camera);
          controls.update()
        };

        window.addEventListener("resize", () => {
          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.setPixelRatio(window.devicePixelRatio)
          renderer.render(scene, camera)
        })

        init()
        animate();
      }
  render() {
    return (
      <div />
    )
  }
}

//const rootElement = document.getElementById("root");
//ReactDOM.render(<App />, rootElement);

export default App;

